# Arduino Make file. Refer to https://github.com/sudar/Arduino-Makefile

ARDUINO_DIR = /usr/share/arduino
ARDUINO_PACKAGE_DIR = $(HOME)/.arduino15/packages
ARDMK_DIR = ./Arduino-Makefile

BOARD_TAG = leonardo
MONITOR_PORT = /dev/ttyACM0

include Arduino-Makefile/Arduino.mk
