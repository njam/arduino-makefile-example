arduino-makefile-example
========================

Compiling and uploading Arduino programs with `make` using [Arduino-Makefile](https://github.com/sudar/Arduino-Makefile).

Development
-----------

### Setup

Download *Arduino-Makefile* as a submodule:
```
git submodule init
git submodule update
```

Install *pySerial* dependency:
```
pip install --user pyserial
```

Install the *Arduino IDE*, *AVR tools* and *AVRDUDE*.
For example on Arch Linux:
```
yay -S arduino arduino-avr-core avr-binutils avr-libc avrdude
```

### Building

Check the `Makefile` for relevant settings.

Compile and upload to the Arduino:
```
make upload
```
